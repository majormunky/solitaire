extends Node2D

const Card = preload("res://Card.tscn")

enum {
	STARTED,
	PLAYING
}

const suits = [
	"clover",
	"diamond",
	"spade",
	"heart"
]

var deck
var cards_on_table = []
var card_pos = Vector2(100, 100)
var state = STARTED
var selected_card = null

func generate_deck() -> Array:
	var result = []
	for suit in suits:
		for i in range(1, 14):
			result.append(make_card(suit, str(i)))
	result.shuffle()
	return result

func _ready() -> void:
	deck = generate_deck()

func deal_klondike() -> void:
	var created_cards = 0
	var flip_cards = [0, 6, 11, 15, 18, 20]
	var pos = Vector2(card_pos.x, card_pos.y)
	var column_count = 6
	for row in range(6):
		var cards_to_create = column_count - row
		var cards_to_skip = column_count - cards_to_create
		for _blank in range(cards_to_skip):
			pos.x += 150
		for col in range(cards_to_create):
			if created_cards in flip_cards:
				print("flip this card (", row, ", ", col, ")")
				deal_card(pos, true)
			else:
				deal_card(pos, false)
			pos.x += 150
			created_cards += 1
			yield(get_tree().create_timer(0.1), "timeout")
		pos.x = card_pos.x
		pos.y += 75
	state = PLAYING

func deal_card(pos: Vector2, is_flipped: bool) -> void:
	var card = deck.pop_front()
	add_child(card)
	card.setup(pos, is_flipped)
	cards_on_table.append(card)
	card.connect("card_selected", self, "_on_card_selected")
	card.connect("card_selected", self, "_on_card_deselected")

func _on_card_selected(card: Node2D):
	if selected_card:
		card.deselect_card()
		if selected_card.color == card.color:
			print("Cards were the same color, returning")
			return
		if int(selected_card.value) - int(card.value) == -1:
			selected_card.set_position(
				Vector2(
					card.global_position.x,
					card.global_position.y + 75
				)
			)
	else:
		print("We don't ahve any cards selected, adding this one")
		selected_card = card

func _on_card_deselected(card: Node2D):
	print("Table noticed a card was deselected")

func make_card(suit: String, value: String) -> Node2D:
	var new_card = Card.instance()
	new_card.set_card(suit, value)
	return new_card

func _on_StartGameButton_button_up() -> void:
	if state == STARTED:
		deal_klondike()
