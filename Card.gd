extends Node2D

onready var sprite = $Sprite
onready var highlight = $Highlight
var suit = "clover"
var value = 1
var color = "black"
var texture_path: String
var back_texture = preload("res://assets/card_back.png")
var flipped = true
var selected = false

signal card_selected(card)
signal card_deselected(card)

func set_card(new_suit: String, new_value: String) -> void:
	suit = new_suit
	value = new_value
	if new_suit == "heart" or new_suit == "diamond":
		color = "red"
	else:
		color = "black"
	texture_path = "res://assets/card_" + new_value + "_" + new_suit + ".png"

func setup(pos: Vector2, is_flipped: bool) -> void:
	set_position(pos)
	flipped = is_flipped
	if flipped:
		sprite.texture = load(texture_path)
	else:
		sprite.texture = back_texture

func flip_card() -> void:
	flipped = not flipped
	if flipped:
		sprite.texture = load(texture_path)
	else:
		sprite.texture = back_texture

func select_card() -> void:
	selected = true
	highlight.visible = true
	emit_signal("card_selected", self)

func deselect_card() -> void:
	selected = false
	highlight.visible = false
	emit_signal("card_deselected", self)

func _on_Area2D_input_event(_viewport: Node, event: InputEvent, _shape_idx: int) -> void:
	if event is InputEventMouseButton and event.is_pressed():
		print("Click")
		if self.flipped:
			print("Clicked on me", suit, value)
			if selected:
				print("Card is selected, trying to deselect")
				deselect_card()
			else:
				print("Card is not selected, going to select it now")
				select_card()
